# Plateforme RShiny de Diagnostic Trafic

[![CeCILL-C license](https://img.shields.io/badge/Licence-CeCILL--C-blue.svg)](https://cecill.info/licences.fr.html)
[![Version](https://img.shields.io/badge/Version-v1.0-green.svg)](#)
[![Maintenance](https://img.shields.io/maintenance/yes/2021)](#)

[![Essayez le en ligne !!!!](https://img.shields.io/badge/testez--le%20sur-dataviz.cerema.fr-orange)](https://dataviz.cerema.fr/securite-routiere-series)

## Sommaire

- [Introduction](#introduction)
- [Installation](#installation)
- [Utilisation](#utilisation)
- [A propos](#a-propos)
---
## Introduction

Dans le cadre d'une étude sur les effets générationnels sur la mortalité routière, le Cerema a créé une plateforme de visualisation de la mortalité routière depuis l'année 1968.
Le tableau de bord propose des analyses avec les variables suivantes : l'année, le mode, l'âge, le sexe, le département de l'accident.

---
## Installation

Cloner le répertoire : 

```shell
git clone https://gitlab.cerema.fr/centre-est/Securite-routiere-series.git
```

---
## Utilisation

### Lancement

#### En local

1. Lancer `Appli_SerieLongue_SR.RProj`

2. Dans R Studio, ouvrir `app.R`

3. Cliquer sur "Run App"

#### En ligne

Aller sur [https://dataviz.cerema.fr/securite-routiere-series](https://dataviz.cerema.fr/securite-routiere-series)

---
## A propos

Contributeurs de l'application :
- nicolas.pele@cerema.fr

Contributeurs sur la base de données amont : 
- anne-sarah.bernagaud@cerema.fr
- laurent.monfront@cerema.fr
- samuel.melennec@cerema.fr 

Pour faire une suggestion, ouvrir une issue avec le tag `suggestion`




