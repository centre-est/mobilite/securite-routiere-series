### Mentions légales
 

Ce tableau de bord est une publication du Centre d’études et d’expertise sur les risques, l’environnement, la mobilité et l’aménagement : Cerema.

### Présentation du site
Ce site présente les données de mortalité routière en France métropolitaine depuis l'année 1968. 

### Utilisation du site
L’accès au site peut être interrompu à tout moment, sans préavis, notamment en cas de force majeure, de maintenance ou si le Cerema décide de suspendre ou d’arrêter la fourniture de ce service.

### Contenu du site
Le Cerema s’efforce d’assurer au mieux de ses possibilités l’exactitude et la mise à jour des informations diffusées, au moment de leur mise en ligne sur le site. Cependant le Cerema ne garantit en aucune façon l’exactitude, la précision ou l’exhaustivité des informations mises à disposition sur le site. Les informations présentes sur le site sont non-contractuelles et peuvent être modifiées à tout moment.

Le site est régi par la loi française. Les utilisateurs étrangers acceptent formellement l’application de la loi française en visitant ce site et en utilisant tout ou partie des fonctionnalités du site. La responsabilité du Cerema ne pourra en aucun cas être engagée quant au contenu et aux informations figurant sur son site web, ou aux conséquences pouvant résulter de leur utilisation ou interprétation.

### Propriété intellectuelle
Les contenus, les textes, y compris les communiqués de presse, les vidéos, les images, les photos et les animations qui apparaissent ou sont disponibles sur le site, sont protégés par le droit de la propriété intellectuelle et sont la propriété du Cerema et du ministère de l’Écologie et du développement durable. Ils sont sous Licence Ouverte. 

A ce titre, vous vous engagez à ne pas copier, traduire, reproduire, commercialiser, publier, exploiter et diffuser partiellement ou intégralement des contenus du site protégés par le droit de la propriété intellectuelle, sans mention du producteur de ce site, à savoir le Cerema et sans mention de la source des données, à savoir l'ONISR.

Les marques et les logos présents sur le site sont, sauf cas particulier, la propriété du Cerema.

La violation de l’un de ces droits de propriété intellectuelle est un délit de contrefaçon passible de deux ans d’emprisonnement et de 150.000 euros d’amende.

### Vie privée
Le Cerema s’engage à ne pas divulguer à des tiers les informations que l’internaute lui communique. Celles-ci sont confidentielles et ne seront utilisées que pour les besoins du service. Le site répond à la réglementation relative à l’informatique, aux fichiers et aux libertés telle que définie par la loi n°78-17 du 6 janvier 1978 modifiée.

### Liens
Le site peut contenir des liens vers des sites de partenaires ou de tiers. Le Cerema, ne disposant d’aucun moyen pour contrôler ces sites, n’offre aucune garantie quant au respect par ces sites des lois et règlement en vigueur.

Le site du Cerema autorise la mise en place d’un lien hypertexte pointant vers son contenu sous réserve de :

- ne pas utiliser la technique du « lien profond » : les pages Cerema ne doivent pas être imbriquées à l’intérieur des pages d’un autre site, mais accessibles par l’ouverture d’une fenêtre séparée

- d’indiquer impérativement dans le lien la source Cerema

- n’utiliser les informations du site Cerema qu’à des fins personnelles, professionnelles ou associatives à l’exclusion de toute utilisation à des fins commerciales ou publicitaires.

Cette autorisation ne s’applique que pour les sites dont le contenu n’est contraire ni à la loi ni aux bonnes mœurs, et dans la mesure où ces liens ne contreviennent pas aux intérêts du Cerema et garantissent pour l’utilisateur la possibilité d’identifier l’origine et l’auteur du document.

### Modification
Le Cerema se réserve le droit de modifier, sans préavis, les présentes conditions générales d’utilisation du site. Pour toute remarque sur le présent site, vous pouvez nous écrire en recourant au formulaire de contact.

### Droit applicable
Le présent site est soumis au droit français.

### Hébergement
Ce site est hébergé sur la plateforme shinyapp.io et administré par le Cerema.

### Conception
Ce site a été conçu et développé par le Cerema.
