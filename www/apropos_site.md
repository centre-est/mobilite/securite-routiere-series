### À propos ce site
Ce site présente les données de mortalité routière en France métropolitaine depuis l'année 1968. 
L'objectif est de fournir un panorama des évolutions de la mortalité routière sur plus de 50 ans.

### Sources de données
Ce site intègre des données issues des fichiers BAAC : Bulletins d'Analyse des Accidents Corporels enregistrés par les forces de l'ordre. Les indicateurs présentés ici et construits à partir de ces données sont labellisés par l'Autorité de la statistique publique par avis initial du 4 juin 2013, publié au JO du 18 juin 2013, puis renouvelé par avis du 21 novembre 2019 publié au JO du 27 novembre 2019. 
Source : Observatoire national interministériel de la sécurité routière (ONISR).

Les données de population utilisées pour calculer le nombre de personnes tuées par million d'habitants sont disponibles sur le site de l'Insitut national de la statistique et des études économiques (INSEE) : [www.insee.fr](https://www.insee.fr).

### Liens
Pour plus d'informations sur la securite routière, consulter le site de l'Observatoire national interministériel de la sécurité routière (ONISR) : [www.onisr.securite-routiere.gouv.fr](https://www.onisr.securite-routiere.gouv.fr).