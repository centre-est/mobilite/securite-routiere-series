### Redressement des données
En 2005, la définition du tué « personne décédée sur le coup ou dans les 6 jours après l’accident » change et devient « personne décédée sur le coup ou dans les 30 jours après l’accident » et la définition du blessé hospitalisé « victime admise comme patient dans un hôpital plus de 6 jours » devient « victime admise comme patient dans un hôpital plus de 24 heures ».

Ce changement de définition créant une rupture de série dans le compte des tués, une estimation du nombre des tués à 30 jours à partir du nombre des tués à 6 jours, par application du coefficient majorateur, permet d’afficher la série longue de 1968 à 2019.
Ce coefficient vaut 1,09 pour les années antérieures à 1993, 1,057 entre 1993 et 2003 et 1,069 pour l'année 2004.

Ces coefficients sont issus des travaux de l'Université Gustave Eiffel.

### Type d'usager
Les différents types d'usagers sont :
- les piétons (incluant EPD sans moteur à partir de 2019)
- les cyclistes (incluant les bicyclettes et EDP avec moteur à partir de 2019)
- les cyclomotoristes (incluant les cyclomoteurs, scooters et 3RM ayant une cylindré inférieure ou égale à 50 cm3)
- les motards (incluant les scooters, motos, side-car et 3RM ayant une cylindré supérieure à 50 cm3)
- les automobilistes (incluant les voitures de tourisme)
- les usagers de véhicules utilitaires (incluant les véhicules utilitaires ayant un PTAC compris entre 1,5 et 3,5 T)
- les usagers de poids lourds
- les autres usagers.

### Estimation de l'âge
Entre 1977 et 1992, seule l'année de naissance est connue. Une hypothèse sur l'âge de l'usager a donc été réalisée de la manière suivante : 
- si l'accident est survenu durant les 6 premiers mois de l'année, son âge est égal à la différence entre l'année de l'accident - l'année de naissance - 1 
- si l'accident est survenu durant les 6 derniers mois de l'année, son âge est égal à la différence entre l'année de l'accident - l'année de naissance


### Département de l'accident
Le département indiqué correspond au département où a eu lieu l'accident.

Pour les accidents survenus en Corse avant 1977, il n'était pas fait mention de la différence entre Corse-du-Sud et Haute-Corse, ce qui explique l'absence de données à l'échelle départementale pour ces deux départements avant 1977 (création des départements Corse-du-Sud et de Haute-Corse par la loi du 15 mai 1975). 
Ces accidents sont en revanche bien comptabilisés lors des analyses à l'échelle nationale (France métropolitaine).